﻿using UnityEngine;

namespace KnifeHitClone.EventSystem
{
    public enum HitType
    {
        ToTarget,
        ToKnife,
        ToApple,
        Other
    }

    public class HitEventArgs
    {
        public HitType HitType;
        public Game.Knife Knife;
        public Collider HitCollider;
        public HitEventArgs(HitType hitType, Game.Knife knife, Collider hitCollider)
        {
            HitType = hitType;
            Knife = knife;
            HitCollider = hitCollider;
        }
    }

    public interface IHitHandler : ISubscriber
    {
        void OnHit(HitEventArgs e);
    }
}
