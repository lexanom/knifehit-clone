﻿using UnityEngine;

namespace KnifeHitClone.EventSystem
{
    public interface ITapHandler : ISubscriber
    {
        void OnTap();
    }
}
