﻿using UnityEngine;

namespace KnifeHitClone.EventSystem
{
    public interface IEndGameHandler : ISubscriber
    {
        void OnGameEnd(bool isWin);
    }
}
