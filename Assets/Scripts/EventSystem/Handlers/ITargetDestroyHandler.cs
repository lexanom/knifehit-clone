﻿using UnityEngine;

namespace KnifeHitClone.EventSystem
{
    public interface ITargetDestroyHandler : ISubscriber
    {
        void OnTargetDestroy(Transform target);
    }
}
