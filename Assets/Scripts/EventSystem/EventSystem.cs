﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace KnifeHitClone.EventSystem
{
    public interface ISubscriber { }

    public class ES
    {
        static readonly Dictionary<Type, List<ISubscriber>> subscribs
            = new Dictionary<Type, List<ISubscriber>>();
        
        public static void Subscribe(ISubscriber subscriber)
        {
            foreach (Type t in GetInterfaces(subscriber))
            {
                if (!subscribs.ContainsKey(t))
                    subscribs[t] = new List<ISubscriber>();
                subscribs[t].Add(subscriber);
            }
        }

        public static void Unsubscribe(ISubscriber subscriber)
        {
            foreach (Type t in GetInterfaces(subscriber))
                if (subscribs.ContainsKey(t))
                    subscribs[t].Remove(subscriber);
        }

        public static void Invoke<T>(Action<T> action) where T : ISubscriber
        {
            if (subscribs.TryGetValue(typeof(T), out List<ISubscriber> subs))
                foreach (ISubscriber subscriber in subs.ToArray())
                    try
                    {
                        action?.Invoke((T)subscriber);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Action invoke for {typeof(T)} ({subscriber.GetType()}) error: " + e);
                    }
        }

        static List<Type> GetInterfaces(ISubscriber subscriber) =>
             subscriber
                .GetType()
                .GetInterfaces()
                .Where(x => x != typeof(ISubscriber))
                .ToList();
    }
}
