﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace KnifeHitClone.Game
{
    [ExecuteInEditMode]
    public class Explose : MonoBehaviour
    {
        [SerializeField] float Force = 1;
        [SerializeField] float Radius = 1;
        [SerializeField] float Upwards = 1;
        [SerializeField] int DurationFrames = 1;

        [SerializeField] Rigidbody[] Items;

        private void Awake()
        {
            if (Items is null || Items.Length == 0)
                Items = GetComponentsInChildren<Rigidbody>();
        }

        void Start()
        {
            StartCoroutine(Explode());
        }

        IEnumerator Explode()
        {
            for (int i = 0; i < DurationFrames; i++)
                foreach (var item in Items)
                {
                    item.AddExplosionForce(Force, transform.position, Radius, Upwards);
                    if (i == 0)
                        item.AddTorque(new Vector3(Rand, Rand, Rand));
                }

            yield return new WaitForSeconds(3);
            Destroy(gameObject);
        }

        float Rand => Random.Range(-100, 100);
    }
}