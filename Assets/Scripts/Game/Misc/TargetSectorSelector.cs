﻿using System.Linq;
using UnityEngine;

namespace KnifeHitClone.Game
{
    public class TargetSectorSelector
    {
        public int Count => Sectors.Length;
        private readonly bool[] Sectors;

        public TargetSectorSelector(int sectorCount)
        {
            Sectors = new bool[sectorCount];
        }

        /// <summary>
        /// Возвращает случайный незанятый сектор или -1, если свободных нет
        /// </summary>
        public int GetNext()
        {
            if (Sectors.Any(x => !x))
            {
                while (true)
                {
                    var i = Random.Range(0, Sectors.Length);
                    if (!Sectors[i])
                    {
                        Sectors[i] = true;
                        return i;
                    }
                }
            }
            else
                return -1;
        }
    }
}