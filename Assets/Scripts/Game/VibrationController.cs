﻿using KnifeHitClone.EventSystem;
using KnifeHitClone.System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.Game
{
    public class VibrationController : MonoBehaviour, IHitHandler, ITargetDestroyHandler
    {
        [SerializeField] VibrationList Vibrations;

        private IVibration Vibration;

        private void OnEnable() =>
            ES.Subscribe(this);

        private void OnDisable() =>
            ES.Unsubscribe(this);

        private void Awake()
        {
            Vibration = new BFVibration();
        }

        public void OnHit(HitEventArgs e)
        {
            switch (e.HitType)
            {
                case HitType.ToTarget:
                    Vibration.Vibrate(Vibrations.TargetHit);
                    break;
                case HitType.ToKnife:
                    Vibration.Vibrate(Vibrations.KnifeHit);
                    break;
                case HitType.ToApple:
                    Vibration.Vibrate(Vibrations.AppleHit);
                    break;
            }
        }

        public void OnTargetDestroy(Transform target)
        {
            Vibration.Vibrate(Vibrations.TargetDestroy);
        }
    }
}