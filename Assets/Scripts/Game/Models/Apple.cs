﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KnifeHitClone.EventSystem;
using System.Threading.Tasks;
using System;

namespace KnifeHitClone.Game
{
    [ExecuteAlways]
    public class Apple : MonoBehaviour, IHitHandler, ITargetDestroyHandler
    {
        [SerializeField] Transform DestroyObjectPrefab;
        [SerializeField] Rigidbody Rigidbody;

        private void OnEnable() =>
            ES.Subscribe(this);

        private void OnDisable() =>
            ES.Unsubscribe(this);

        private void Start()
        {
            if (!Rigidbody)
                Rigidbody = GetComponent<Rigidbody>();
        }

        public void OnHit(HitEventArgs e)
        {
            if (e.HitType == HitType.ToApple && e.HitCollider.transform == transform)
            {
                Instantiate(DestroyObjectPrefab, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }

        public void OnTargetDestroy(Transform target)
        {
            Rigidbody.isKinematic = false;
            Rigidbody.useGravity = true;
        }
    }
}