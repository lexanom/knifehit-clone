﻿using System.Collections;
using UnityEngine;
using KnifeHitClone.EventSystem;

namespace KnifeHitClone.Game
{
    public class Knife : MonoBehaviour, ITargetDestroyHandler
    {
        [SerializeField] Collider Collider;
        [SerializeField] Rigidbody Rigidbody;

        public enum KnifeStatus
        {
            None,
            Throwed,
            Hitted,
            Bounced
        }

        public Vector3 Velocity { get; private set; }
        public KnifeStatus Status { get; private set; }
        public float Speed => Velocity.magnitude;

        private void OnEnable() =>
            ES.Subscribe(this);

        private void OnDisable() =>
            ES.Unsubscribe(this);

        private void Awake()
        {
            if (!Collider)
                Collider = GetComponent<Collider>();
            if (!Rigidbody)
                Rigidbody = GetComponent<Rigidbody>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (Collider.isTrigger && Status > KnifeStatus.None)
            {
                HitType htype;
                if (other.CompareTag("Target"))
                {
                    EndCheckCollision();
                    htype = HitType.ToTarget;
                }
                else if (other.CompareTag("Knife"))
                {
                    EndCheckCollision();
                    htype = HitType.ToKnife;
                }
                else if (other.CompareTag("Apple"))
                    htype = HitType.ToApple;
                else
                    htype = HitType.Other;

                ES.Invoke<IHitHandler>(x => x.OnHit(new HitEventArgs(htype, this, other)));
                //Hit?.Invoke(new OnHitArgs(this, other));
            }
        }

        /// <summary>
        /// Отскок ножа
        /// </summary>
        /// <param name="bouncePos">Точка отскока</param>
        /// <param name="bounceSpeed">Скорость отскока</param>
        public void Bounce(Vector3 bouncePos, float bounceSpeed)
        {
            Status = KnifeStatus.Bounced;

            Velocity = (transform.position - bouncePos).normalized * bounceSpeed;

            Rigidbody.isKinematic = false;
            Rigidbody.useGravity = true;

            Rigidbody.velocity = Velocity * 50; 
            Rigidbody.AddTorque(new Vector3(
                Random.Range(-360, 360),
                Random.Range(-360, 360),
                Random.Range(-360, 360)));
        }

        /// <summary>
        /// Бросок ножа
        /// </summary>
        /// <param name="speed"></param>
        public void Push(float speed)
        {
            if (Status == KnifeStatus.None)
            {
                Status = KnifeStatus.Throwed;
                Collider.isTrigger = true;

                StartCoroutine(Throw(speed));
            }
        }

        /// <summary>
        /// Защита от "дребезга"
        /// </summary>
        void EndCheckCollision()
        {
            Status = KnifeStatus.Hitted;
            Collider.isTrigger = false;
        }

        private IEnumerator Throw(float speed)
        {
            var TargetSpeed = transform.up * speed;
            while (Status == KnifeStatus.Throwed)
            {
                Velocity = Vector3.Lerp(Velocity, TargetSpeed, Time.deltaTime * 10);
                transform.Translate(Velocity, Space.World);
                yield return new WaitForEndOfFrame();
            }
        }

        /// <summary>
        /// Выход за пределы камеры
        /// </summary>
        private void OnBecameInvisible()
        {
            if (Status > KnifeStatus.None)
                Destroy(gameObject);
        }

        public void OnTargetDestroy(Transform target)
        {
            Bounce(target.position, Random.Range(0.01f, 0.05f));
        }
    }
}