﻿using KnifeHitClone.EventSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.Game
{
    public class EffectsController : MonoBehaviour, IHitHandler
    {
        [SerializeField] ParticleSystem KnifeHitEffect;
        [SerializeField] ParticleSystem TargetHitEffect;
        [SerializeField] ParticleSystem AppleHitEffect;
        [SerializeField] float TargetDeformModificator = 20;
        [SerializeField] float TargetDeformScale = .8f;

        private void OnEnable() =>
            ES.Subscribe(this);

        private void OnDisable() =>
            ES.Unsubscribe(this);

        private ParticleSystem targetHitPart;
        private Coroutine targetRoutine;

        public void OnHit(HitEventArgs e)
        {
            if (Storage.Settings.UseFX)
                switch (e.HitType)
                {
                    case HitType.ToTarget:
                        if (!targetHitPart)
                        {
                            targetHitPart = Instantiate(TargetHitEffect);
                            var rend = e.HitCollider.transform.GetComponentInChildren<Renderer>();
                            if (rend)
                            {
                                var main = targetHitPart.main;
                                main.startColor = rend.material.color;
                            }
                            targetHitPart.transform.SetParent(e.HitCollider.transform);
                        }

                        var normal = (e.HitCollider.transform.position - e.Knife.transform.position).normalized;
                        targetHitPart.transform.position = e.HitCollider.transform.position - normal / 2;
                        targetHitPart.transform.LookAt(e.HitCollider.transform.position - normal);
                        targetHitPart.Play();

                        if (targetRoutine is null)
                            targetRoutine = StartCoroutine(TargetDeform(e.HitCollider.transform));

                        break;

                    case HitType.ToKnife:
                        KnifeHitEffect.transform.position = (e.HitCollider.transform.position + e.Knife.transform.position) / 2;
                        KnifeHitEffect.Play();
                        break;

                    case HitType.ToApple:
                        AppleHitEffect.transform.position = e.HitCollider.transform.position;
                        AppleHitEffect.Play();
                        break;
                }
        }

        /// <summary>
        /// Вспышка и деформация мишени
        /// </summary>
        IEnumerator TargetDeform(Transform transform)
        {
            var scale = transform.localScale;

            var rend = transform.GetComponentInChildren<Renderer>();
            var emColor = Color.white;
            var _em = "_EMISSION";
            var _emColor = "_EmissionColor";
            if (rend)
            {
                rend.material.EnableKeyword(_em);
                rend.material.SetColor(_emColor, emColor);
            }

            for (float i = 0; i < 2; i += Time.deltaTime * TargetDeformModificator)
            {
                if (i <= 1)
                    transform.localScale = Vector3.Lerp(scale, scale * TargetDeformScale, i);
                else
                    transform.localScale = Vector3.Lerp(scale * TargetDeformScale, scale, i - 1);

                if (rend)
                    rend.material.SetColor(_emColor, Color.Lerp(emColor, Color.clear, i / 2));
                yield return null;
            }

            if (rend)
                rend.material.DisableKeyword(_em);
            transform.localScale = scale;
            targetRoutine = null;
        }
    }
}