﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KnifeHitClone.EventSystem;
using System.Collections;
using UnityEngine.Networking;
using System.IO;
using System.Threading.Tasks;

namespace KnifeHitClone.Game
{
	[RequireComponent(typeof(AudioSource))]
	public class SoundController : MonoBehaviour, IHitHandler, ITargetDestroyHandler
	{
		/**
			 Методы возспроизведения юнити на андроиде дают задержку звука (>500ms), 
			 поэтому юзаем плагин для воспроизведения нативными средствами
		*/

		[SerializeField] SoundList SoundList;
		[SerializeField] AudioSource AudioSource;

		private void OnEnable() =>
			ES.Subscribe(this);

		private void OnDisable() =>
			ES.Unsubscribe(this);

        async void Play(SoundList.SoundItem item)
		{
			if (Storage.Settings.UseSound)
			{
				await Task.Delay(50);
#if UNITY_EDITOR
				StartCoroutine(Play(item.FileName));
#else
				if (Storage.Settings.UseSound && item.Id >= 0)
					AndroidNativeAudio.play(item.Id);
#endif
			}
        }

        public void OnHit(HitEventArgs e)
		{
            switch (e.HitType)
            {
                case HitType.ToTarget:
                    Play(SoundList.GetHitTarget());
                    break;
                case HitType.ToKnife:
                    Play(SoundList.GetHitKnife());
                    break;
                case HitType.ToApple:
                    Play(SoundList.GetHitApple());
                    break;
            }
        }

        public void OnTargetDestroy(Transform target)
		{
			Play(SoundList.GetTargetDestroy());
		}

#if UNITY_EDITOR
		// id файла возвращается только для андроида, потому ключом юзаем строку
		readonly Dictionary<string, AudioClip> localCache = new Dictionary<string, AudioClip>();

		IEnumerator Play(string soundName)
		{
			if (!localCache.TryGetValue(soundName, out AudioClip clip))
			{
				yield return StartCoroutine(LoadFromAssets(soundName));
				clip = localCache[soundName];
			}
			AudioSource.PlayOneShot(clip);
		}

		IEnumerator LoadFromAssets(string soundName)
		{
			string path = Path.Combine(Application.streamingAssetsPath, soundName);
			using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.WAV))
			{
				yield return www.SendWebRequest();
				if (www.result != UnityWebRequest.Result.Success)
				{
					Debug.LogError("GetAudioClip error: " + www.error);
					yield break;
				}
				localCache[soundName] = DownloadHandlerAudioClip.GetContent(www);
			}
		}
#endif
	}
}