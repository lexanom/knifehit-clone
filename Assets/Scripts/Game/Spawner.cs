﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.Game
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] Knife KnifePrefab;
        [SerializeField] Apple ApplePrefab;

        public void Init(Knife knifePrefab)
        {
            KnifePrefab = knifePrefab;
        }

        public Knife SpawnKnife(bool animate = false)
        {
            var knife = Instantiate(KnifePrefab, transform);
            if (animate)
                StartCoroutine(AnimateSpawn(knife.transform));
            return knife;
        }

        public Apple SpawnApple()
        {
            return Instantiate(ApplePrefab, transform);
        }

        /// <summary>
        /// Плавное выезжание ножа снизу экрана
        /// </summary>
        IEnumerator AnimateSpawn(Transform knife)
        {
            var dest = knife.position;
            knife.position -= knife.up;
            for (float i = 0; i <= 1; i += Time.deltaTime * 10)
            {
                yield return null;
                knife.position = Vector3.Lerp(knife.position, dest, i);
            }
            knife.position = dest;
        }
    }
}