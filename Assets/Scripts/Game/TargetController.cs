﻿using KnifeHitClone.EventSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KnifeHitClone.Game
{
    public class TargetController : MonoBehaviour
    {
        /// <summary>
        /// Описание скорости вращения мишени
        /// </summary>
        [SerializeField] AnimationCurve SpeedCurve;
        [SerializeField] float SpeedMultiplicator = 1;

        private Transform CurrentTarget;
        private Transform DestroyTransform;

        public bool IsActive { get; private set; }

        /// <summary>
        /// Смещение стартовой позиции на кривой вращения
        /// </summary>
        private float randomOffset;

        private void Start()
        {
            randomOffset = UnityEngine.Random.Range(0, 100);
        }

        /// <summary>
        /// Конструктор компонента
        /// </summary>
        public void Init(AnimationCurve speedCurve, Target targetObject)
        {
            IsActive = true;
            SetCurve(speedCurve);
            SetObject(targetObject);
        }

        private void SetCurve(AnimationCurve speedCurve)
        {
            SpeedCurve = speedCurve;
            SpeedCurve.postWrapMode = FixWrapMode();
        }

        private void SetObject(Target targetObject)
        {
            if (transform.childCount > 0)
                foreach (Transform t in transform)
                    Destroy(t.gameObject);

            //if (!targetObject)
            //    CurrentTarget = GetComponentInChildren<Target>();
            //else
            CurrentTarget = Instantiate(targetObject.TargetTransform, transform);
            DestroyTransform = targetObject.DestroyTransform;
        }

        internal void Stop()
        {
            IsActive = false;
        }

        /// <summary>
        /// Зацикливаем анимацию вращения если забыли при создании кривой
        /// </summary>
        private WrapMode FixWrapMode()
        {
            switch (SpeedCurve.postWrapMode)
            {
                case WrapMode.Default:
                case WrapMode.Once:
                case WrapMode.ClampForever:
                    return SpeedCurve.postWrapMode = WrapMode.Loop;
            }
            return SpeedCurve.postWrapMode;
        }

        /// <summary>
        /// Закрепить нож в мишени
        /// </summary>
        internal void FixTransform(Transform tr, float relativeAngle = -1)
        {
            tr.transform.SetParent(transform);
            tr.transform.position = transform.position - (transform.position - tr.transform.position).normalized / 2;

            if (relativeAngle >= 0)
                tr.transform.RotateAround(transform.position, transform.forward, relativeAngle);
        }

        private void FixedUpdate()
        {
            if (IsActive)
            {
                float spin = SpeedCurve.Evaluate(Time.time + randomOffset);
                transform.Rotate(transform.forward, spin * SpeedMultiplicator, Space.World);
            }
        }

        internal void DestroyTarget()
        {
            ES.Invoke<ITargetDestroyHandler>(x => x.OnTargetDestroy(transform));

            Stop();

            // заменяем мишень разрушаемым объектом
            if (CurrentTarget)
            {
                if (DestroyTransform)
                    Instantiate(DestroyTransform, transform);
                Destroy(CurrentTarget.gameObject);
            }
        }
    }
}