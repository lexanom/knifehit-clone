﻿using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using KnifeHitClone.EventSystem;
using System;

namespace KnifeHitClone.Game
{
    public class LevelController : MonoBehaviour, IHitHandler, ITapHandler
    {
        [SerializeField] LevelList Levels;
        [SerializeField] KnifeList Knives;
        [SerializeField] GameOptions Options;
        [Space]
        [SerializeField] Spawner Spawner;
        [SerializeField] TargetController Target;
        [Space]
        [SerializeField] UI.LevelUI UIController;
        [Space]
        [SerializeField] UnityEvent AfterWinGameEvent;
        [SerializeField] UnityEvent AfterFailGameEvent;

        private Level CurrentLevel;
        private Knife CurrentKnife;
        private int KnivesCount;

        public int CurrentStage { get; private set; }
        public int Score { get; private set; }
        public bool IsNewBest { get; private set; }

        public enum Mode
        {
            Begin,
            Win,
            Lose
        }
        public Mode PlayMode { get; private set; }

        private void OnEnable() =>
            ES.Subscribe(this);

        private void OnDisable() =>
            ES.Unsubscribe(this);

        private void Start()
        {
            Restart();
        }

        public void Restart()
        {
            Score = 0;
            IsNewBest = false;
            CurrentStage = 0;
            Init();
        }

        public void TryAgain()
        {
            if (Storage.Prefs.SpendApples(Options.TryAgainCost, true))
                Init();
            else
                UIController.ShowError($"Attemp cost {Options.TryAgainCost} apples");
        }

        void Init()
        {
            if (CurrentStage < Levels.List.Length)
                CurrentLevel = Levels.List[CurrentStage];
            else
                CurrentLevel = Levels.List.Last();

            KnivesCount = CurrentLevel.Knives;
            Target.Init(CurrentLevel.RotationCurve, CurrentLevel.Target);

            SpawnLevelObjects();

            Spawner.Init(GetKnive(Storage.Prefs.CurrentKnife));
            SpawnKnife();

            PlayMode = Mode.Begin;

            CheckMaxStage();
            UIController.Init(CurrentLevel, CurrentStage + 1, Score);
        }

        private Knife GetKnive(int id)
        {
            if (id < Knives.List.Count /*&& Storage.Prefs.Knives.Contains(id)*/)
            {
                var k = Knives.List[id].knife;
                if (!k)
                    Debug.LogError($"Не задан Knife.Gameobject (index: {id})");
                return k;
            }
                        
            return null;
        }

        private void SpawnLevelObjects()
        {
            var sectorSelector = new TargetSectorSelector(Options.MaxSpawnItems);

            // ножи в мишени
            if (CurrentLevel.KnivesOnLevel.Min > 0)
            {
                Spawner.Init(GetKnive(UnityEngine.Random.Range(0, Knives.List.Count)));

                if (CurrentLevel.KnivesOnLevel.Max > sectorSelector.Count)
                    CurrentLevel.KnivesOnLevel.Max = sectorSelector.Count;
                var count = UnityEngine.Random.Range(CurrentLevel.KnivesOnLevel.Min, CurrentLevel.KnivesOnLevel.Max);

                for (int i = 0; i < count; i++)
                {
                    var sector = sectorSelector.GetNext();
                    if (sector < 0)
                        break;
                    Target.FixTransform(Spawner.SpawnKnife().transform, 360 / sectorSelector.Count * sector);
                }
            }

            // яблоко
            if (Options.AppleSpawnChance > 0 && UnityEngine.Random.Range(0, 1f) <= Options.AppleSpawnChance)
            {
                var sector = sectorSelector.GetNext();
                if (sector >= 0)
                {
                    Target.FixTransform(Spawner.SpawnApple().transform, 360 / sectorSelector.Count * sector);
                }
            }
        }

        private void CheckMaxStage()
        {
            if (CurrentStage + 1 > Storage.Prefs.MaxStage)
                Storage.Prefs.MaxStage = CurrentStage + 1;
        }

        private void AddScore()
        {
            Score++;
            if (Score > Storage.Prefs.MaxScore)
            {
                Storage.Prefs.MaxScore = Score;
                IsNewBest = true;
            }
            UIController.SetScore(Score);
        }

        void AddApple()
        {
            Storage.Prefs.AddApples(1);
            UIController.UpdateApples();
        }

        private async void WinGame()
        {
            ES.Invoke<IEndGameHandler>(x => x.OnGameEnd(true));
            PlayMode = Mode.Win;

            Target.DestroyTarget();

            await Task.Delay(1000);

            CheckReward();

            if (CurrentStage < Levels.List.Length)
                CurrentStage++;
            AfterWinGameEvent.Invoke();
        }

        private async void FailGame()
        {
            ES.Invoke<IEndGameHandler>(x => x.OnGameEnd(false));

            Target.Stop();
            PlayMode = Mode.Lose;

            await Task.Delay(1000);
            UIController.ShowResult(CurrentStage + 1, Score, IsNewBest);
            AfterFailGameEvent.Invoke();
        }


        private int lastRewardId;
        private void CheckReward()
        {
            if (CurrentLevel.BossSettings.Reward)
            {
                var knife = CurrentLevel.BossSettings.Reward;
                var id = Knives.List.IndexOf(knife);
                if (!Storage.Prefs.Knives.Contains(id))
                {
                    lastRewardId = id;
                    Storage.Prefs.Knives.Add(id);
                    UIController.ShowReward(knife.Icon);
                }
            }
        }

        public void EquipLastReward()
        {
            Storage.Prefs.CurrentKnife = lastRewardId;
            Storage.Save();

            Destroy(CurrentKnife.gameObject);
            Spawner.Init(GetKnive(lastRewardId));
            SpawnKnife();
        }

        [ContextMenu("Spawn new one")]
        private void SpawnKnife()
        {
            CurrentKnife = Spawner.SpawnKnife(true);
        }

        public void OnHit(HitEventArgs e)
        {
            switch (e.HitType)
            {
                case HitType.ToTarget:
                {
                    Target.FixTransform(e.Knife.transform);
                    AddScore();

                    if (KnivesCount == 0)
                        WinGame();
                    else
                        SpawnKnife();
                }
                break;

                case HitType.ToKnife:
                {
                    //Target.Stop();
                    CurrentKnife.Bounce(e.HitCollider.transform.position, e.Knife.Speed);
                    FailGame();
                }
                break;


                case HitType.ToApple:
                {
                    AddApple();
                }
                break;
            }
        }

        public void OnTap()
        {
            if (PlayMode != Mode.Begin)
                return;

            if (CurrentKnife.Status == Knife.KnifeStatus.None)
            {
                CurrentKnife.Push(Options.KnifeSpeed);
                KnivesCount--;
                UIController.SetKnives(KnivesCount);
            }
        }
    }
}