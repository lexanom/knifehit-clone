﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.UI
{
    [RequireComponent(typeof(Animator), typeof(CanvasGroup))]
    public class Screen : MonoBehaviour
    {
        [SerializeField] Animator Animator;
        [SerializeField] OpenAnimation OpenAnim;
        [SerializeField] CloseAnimation CloseAnim;
        [SerializeField, Range(.1f, 1f)] float AnimSpeed = .5f;
        [Space]
        public Camera CustomCamera;

        public delegate void Callback();

        public enum OpenAnimation
        {
            None,
            Appearence
        }

        public enum CloseAnimation
        {
            None,
            Disapearence
        }

        public bool HasOpenAnim => OpenAnim > 0;
        public bool HasCloseAnim => CloseAnim > 0;

        private void OnEnable()
        {
            if (ScreenManager.ScreenCount == 0)
                ScreenManager.SetMainScreen(this);

            if (!Animator)
                Animator = GetComponent<Animator>();

            if (Animator)
            {
                Animator.SetFloat("OpenAnim", (int)OpenAnim);
                Animator.SetFloat("CloseAnim", (int)CloseAnim);
                Animator.SetFloat("SpeedMultiplier", AnimSpeed);
            }

            Show();
        }

        /// <summary>
        /// Добавить окно в стек и показать на экране
        /// </summary>
        public void Push(Screen screen)
        {
            ScreenManager.Push(screen, true);
        }

        /// <summary>
        /// Добавить окно в стек и показать на экране
        /// </summary>
        public void PushModal(Screen screen)
        {
            ScreenManager.Push(screen, false);
        }

        /// <summary>
        /// Удалить последний скрин из стека и заменить следующим
        /// </summary>
        public void ReplaceLast(Screen screen)
        {
            ScreenManager.ReplaceLast(screen);
        }

        /// <summary>
        /// Изъять последнее окно из стека, показать предыдущее
        /// </summary>
        public void Pop()
        {
            ScreenManager.Pop();
        }

        /// <summary>
        /// Очистить стек, переход на стартовую страницу
        /// </summary>
        public void MainScreen()
        {
            ScreenManager.MainScreen();
        }

        Coroutine routine;

        internal void Show(Callback callback = null)
        {
            if (routine != null)
                StopCoroutine(routine);
            routine = StartCoroutine(ActivateWindow(callback));
        }

        internal void Hide(Callback callback = null)
        {
            Animator.SetTrigger("Close");
            if (routine != null)
                StopCoroutine(routine);
            routine = StartCoroutine(DeactivateWindow(callback));
        }

        IEnumerator ActivateWindow(Callback callback)
        {
            gameObject.SetActive(true);
            Animator.enabled = true;
            if (HasOpenAnim)
                while (!Animator.GetCurrentAnimatorStateInfo(0).IsName("Opened"))
                    yield return new WaitForEndOfFrame();
            Animator.enabled = false;
            GetComponent<CanvasGroup>().alpha = 1;
            callback?.Invoke();
        }

        IEnumerator DeactivateWindow(Callback callback)
        {
            Animator.enabled = true;
            if (HasCloseAnim)
                while (!Animator.GetCurrentAnimatorStateInfo(0).IsName("Closed"))
                    yield return new WaitForEndOfFrame();
            Animator.enabled = false;
            callback?.Invoke();
        }
    }
}