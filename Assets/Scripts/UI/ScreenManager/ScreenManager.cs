﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.UI
{
    /// <summary>
    /// Менеджер UI окон представленных префабами, с возможностью переключения камер
    /// </summary>
    public class ScreenManager
    {
        public static int ScreenCount => Stack.Count;
        private static Stack<Screen> Stack = new Stack<Screen>();
        private static Camera MainCamera;

        public static void SetMainScreen(Screen window)
        {
            if (Camera.main)
                MainCamera = Camera.main;
            Stack = new Stack<Screen>();
            Stack.Push(window);
        }

        public static void Push(Screen window, bool HidePrev, bool deletePrev = false)
        {
            var peek = Stack.Peek();

            if (peek == window)
                return;

            var newWindow = GameObject.Instantiate(window);
            newWindow.transform.SetAsLastSibling();

            newWindow.Show(() =>
            {
                if (HidePrev && peek)
                    peek.gameObject.SetActive(false);

                if (!newWindow.CustomCamera && MainCamera)
                {
                    MainCamera.gameObject.SetActive(true);
                    Camera.SetupCurrent(MainCamera);
                }
            });

            if (HidePrev)
                if (newWindow.CustomCamera)
                {
                    var cam = GetCamera();
                    peek.Hide();
                    if (cam)
                        cam.gameObject.SetActive(false);
                    Camera.SetupCurrent(newWindow.CustomCamera);
                }

            if (deletePrev)
            {
                var w = Stack.Pop();
                GameObject.Destroy(w.gameObject);
                CheckCam(newWindow.CustomCamera ?? MainCamera);
            }

            Stack.Push(newWindow);
        }

        public static void Pop()
        {
            if (Stack.Count == 1)
                return;

            var currentWindow = Stack.Pop();
            var peek = Stack.Peek();

            peek.gameObject.SetActive(true);

            if (!peek.CustomCamera)
                MainCamera.gameObject.SetActive(true);
            else if (peek.CustomCamera != MainCamera)
                MainCamera.gameObject.SetActive(false);

            currentWindow.Hide(() =>
            {
                CheckCam(peek.CustomCamera);
                GameObject.Destroy(currentWindow.gameObject);
            });
        }

        internal static void ReplaceLast(Screen screen)
        {
            Push(screen, true, true);
        }

        public static void MainScreen()
        {
            if (Stack.Count == 1)
                return;

            var peek = Stack.Peek();

            Screen newWindow = null;
            while (Stack.Count > 0)
            {
                newWindow = Stack.Pop();
                if (Stack.Count > 0)
                    GameObject.Destroy(newWindow.gameObject);
            }

            newWindow.gameObject.SetActive(true);
            if (MainCamera)
            {
                MainCamera.gameObject.SetActive(true);
                Camera.SetupCurrent(MainCamera);
            }
            GameObject.Destroy(peek.gameObject);
        }

        static void CheckCam(Camera newCam)
        {
            var cam = GetCamera();
            if (!newCam)
            {
                MainCamera.gameObject.SetActive(true);
                Camera.SetupCurrent(MainCamera);
            }
            else if (newCam != cam)
            {
                cam.gameObject.SetActive(false);
                newCam.gameObject.SetActive(true);
                Camera.SetupCurrent(newCam);
            }
        }

        static Camera GetCamera()
        {
            var cam = Camera.current;
            if (!cam)
                cam = Camera.main;
            return cam;
        }
    }
}