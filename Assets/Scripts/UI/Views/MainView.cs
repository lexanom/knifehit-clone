﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    public class MainView : MonoBehaviour
    {
        [SerializeField] KnifeList Knives;
        [SerializeField] Image Knife;

        private void OnEnable()
        {
            Knife.sprite = Knives.List[Storage.Prefs.CurrentKnife].Icon;
        }
    }
}