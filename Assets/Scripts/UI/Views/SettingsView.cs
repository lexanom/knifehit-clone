﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using UnityEngine.UI;
using KnifeHitClone.System;

namespace KnifeHitClone.UI
{
    public class SettingsView : MonoBehaviour
    {
        [SerializeField] Toggle soundToggle;
        [SerializeField] Toggle vibrationToggle;
        [SerializeField] Toggle fxToggle;

        private void OnEnable()
        {
            soundToggle.isOn = Storage.Settings.UseSound;
            vibrationToggle.isOn = Storage.Settings.UseVibration;
            fxToggle.isOn = Storage.Settings.UseFX;
        }

        private void OnDisable()
        {
            Storage.Settings.UseSound = soundToggle.isOn;
            Storage.Settings.UseVibration = vibrationToggle.isOn;
            Storage.Settings.UseFX = fxToggle.isOn;
        }

        public void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }
    }
}