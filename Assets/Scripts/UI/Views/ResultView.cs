﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    class ResultView : MonoBehaviour
    {
        [SerializeField] AiryUIAnimationManager Animator;
        [SerializeField] GameObject NewBest;
        [SerializeField] Text Stage;
        [SerializeField] Text Score;

        internal void Init(int stage, int score, bool newbest)
        {
            Stage.text = $"STAGE {stage}";
            Score.text = $"Score: {score}";
            NewBest.SetActive(newbest);
            Animator.ShowMenu();
            transform.SetAsLastSibling();
        }
    }
}
