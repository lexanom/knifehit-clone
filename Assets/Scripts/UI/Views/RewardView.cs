﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    class RewardView : MonoBehaviour
    {
        [SerializeField] AiryUIAnimationManager Animator;
        [SerializeField] Image Image;

        internal void Init(Sprite sprite)
        {
            Image.sprite = sprite;
            transform.SetAsLastSibling();
            Animator.ShowMenu();
        }
    }
}
