﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    public class KnivesView : MonoBehaviour
    {
        [SerializeField] KnifeList Knives;
        [SerializeField] Transform Content;
        [SerializeField] KnifeUIItem ItemPrefab;
        [Space]
        [SerializeField] ModalWindow ModalEquip;
        [SerializeField] ModalWindow ModalBuy;
        [SerializeField] ModalWindow ModalError;

        public class Item
        {
            public int Id;
            public Knife knife;
            public bool IsActive;
            public bool isSelected;
        }

        private Item selectedItem;

        private void Awake()
        {
            RebuildContent();
        }

        private void Click(Item item)
        {
            selectedItem = item;
            if (!item.IsActive)
            {
                ModalBuy.Text = $"Buy knife for {item.knife.Cost} apples?";
                ModalBuy.Show();
            }
            else if (!item.isSelected)
                ModalEquip.Show();
        }

        private void RebuildContent()
        {
            if (Content.childCount > 0)
                foreach (Transform t in Content.transform)
                    Destroy(t.gameObject);

            for (int i = 0; i < Knives.List.Count; i++)
            {
                var item = new Item
                {
                    Id = i,
                    knife = Knives.List[i],
                    IsActive = Storage.Prefs.Knives.Contains(i),
                    isSelected = Storage.Prefs.CurrentKnife == i
                };

                Instantiate(ItemPrefab, Content).Init(item, () => Click(item));
            }
        }

        public void Buy()
        {
            if (Storage.Prefs.Knives.Contains(selectedItem.Id))
            {
                ShowError("Already buyed");
                return;
            }

            if (!Storage.Prefs.SpendApples(selectedItem.knife.Cost, false))
            {
                ShowError("Not enough apples");
                return;
            }

            Storage.Prefs.Knives.Add(selectedItem.Id);
            Storage.Prefs.CurrentKnife = selectedItem.Id;
            Storage.Save();

            RebuildContent();
        }

        public void Equip()
        {
            if (!Storage.Prefs.Knives.Contains(selectedItem.Id))
            {
                ShowError("Buy this");
                return;
            }
            Storage.Prefs.CurrentKnife = selectedItem.Id;
            Storage.Save();

            RebuildContent();
        }

        private void ShowError(string text)
        {
            ModalBuy.Hide();
            ModalError.Text = text;
            ModalError.Show();
        }
    }
}