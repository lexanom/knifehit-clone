﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone.UI
{
    public class ScoreUI : MonoBehaviour
    {
        [SerializeField] TMPro.TMP_Text Score;
        [SerializeField] TMPro.TMP_Text Apples;
        [SerializeField] TMPro.TMP_Text MaxStage;

        private void OnEnable()
        {
            Score.text = Storage.Prefs.MaxScore.ToString();
            Apples.text = Storage.Prefs.Apples.ToString();
            MaxStage.text = "STAGE " + Storage.Prefs.MaxStage;
        }
    }
}