﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    public class LevelUI : MonoBehaviour
    {
        [SerializeField] TMPro.TMP_Text StageText;
        [SerializeField] TMPro.TMP_Text ScoreText;
        [SerializeField] TMPro.TMP_Text ApplesText;
        [Space]
        [SerializeField] Transform KnivesContainer;
        [SerializeField] Image KnifePrefab;
        [Space]
        [SerializeField] Renderer Background;
        [SerializeField] Color NormalBackground;
        [SerializeField] Color BossBackground;
        [Space]
        [SerializeField] ModalWindow ModalError;
        [Space]
        [SerializeField] ResultView ResultView;
        [SerializeField] RewardView RewardView;
        [SerializeField] AiryUIAnimationManager BossIntro;

        private List<Image> Knives = new List<Image>();

        private void Awake()
        {
            ResultView.gameObject.SetActive(false);
            RewardView.gameObject.SetActive(false);
            BossIntro.gameObject.SetActive(false);
        }

        public void Init(Level Level, int levelNum, int score)
        {
            InitKnives(Level.Knives);
            SetStageName(Level, levelNum);
            SetScore(score);
            UpdateApples();
            InitBackground(Level.LevelType);

            if (Level.LevelType > LevelType.Normal)
                ShowBossIntro();
        }

        private void InitBackground(LevelType levelType)
        {
            Background.material.color = levelType == LevelType.Normal
                ? NormalBackground
                : BossBackground;
        }

        private void SetStageName(Level Level, int levelNum)
        {
            if (Level.LevelType > LevelType.Normal)
                StageText.text = $"{Level.LevelType}:  {Level.BossSettings.Name}";
            else
                StageText.text = $"Stage {levelNum}";
        }

        private void InitKnives(int knives)
        {
            if (Knives.Any())
                foreach (var item in Knives)
                    Destroy(item.gameObject);

            Knives = new List<Image>();
            for (int i = 0; i < knives; i++)
                Knives.Add(Instantiate(KnifePrefab, KnivesContainer));
        }

        internal void ShowError(string text)
        {
            ModalError.Text = text;
            ModalError.Show();
        }

        internal void SetKnives(int knives)
        {
            for (int i = 0; i < Knives.Count; i++)
                Knives[i].color = i < knives ? Color.white : Color.black;
        }

        public void SetScore(int count)
        {
            ScoreText.text = count.ToString();
        }

        public void UpdateApples()
        {
            ApplesText.text = Storage.Prefs.Apples.ToString();
        }

        internal void ShowReward(Sprite rewardIcon)
        {
            RewardView.Init(rewardIcon);
        }

        public void ShowResult(int stage, int score, bool newbest)
        {
            ResultView.Init(stage, score, newbest);
        }

        void ShowBossIntro()
        {
            BossIntro.transform.SetAsLastSibling();
            BossIntro.ShowMenu();
        }
    }
}