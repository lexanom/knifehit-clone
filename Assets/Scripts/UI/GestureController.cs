﻿using KnifeHitClone.EventSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace KnifeHitClone.UI
{
    public class GestureController : MonoBehaviour, IPointerDownHandler
    {
        private void Start()
        {
            transform.SetAsLastSibling();
        }

        //private void Update()
        //{
        //    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        //        CheckTouch(Input.GetTouch(0).position);

        //    if (Input.GetMouseButtonDown(0))
        //        CheckTouch(Input.mousePosition);
        //}

        /// <summary>
        /// Будем ловить тап только в зоне, ограниченной рамками объекта
        /// </summary>
        public void OnPointerDown(PointerEventData eventData)
        {
            ES.Invoke<ITapHandler>(x => x.OnTap());
            //Tap?.Invoke();
        }
    }
}