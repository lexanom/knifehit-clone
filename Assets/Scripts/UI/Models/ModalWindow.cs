﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    class ModalWindow : MonoBehaviour
    {
        [SerializeField] AiryUIAnimationManager Animator;
        [SerializeField] Text title;
        [SerializeField] Text text;
        [SerializeField] Text okButtonText;
        [SerializeField] Text cancelButtonText;
        [SerializeField] Button OkButton;

        public string Title
        {
            get { return title.text; }
            set { title.text = value; }
        }

        public string Text
        {
            get { return text.text; }
            set { text.text = value; }
        }

        public string OkButtonText
        {
            get { return okButtonText.text; }
            set { okButtonText.text = value; }
        }

        public string CancelButtonText
        {
            get { return cancelButtonText.text; }
            set { cancelButtonText.text = value; }
        }

        public UnityAction Callback
        {
            set
            {
                OkButton.onClick.RemoveAllListeners();
                OkButton.onClick.AddListener(value);
            }
        }

        public void Show()
        {
            transform.SetAsLastSibling();
            Animator.ShowMenu();
        }

        public void Hide() => Animator.HideMenu();
    }
}
