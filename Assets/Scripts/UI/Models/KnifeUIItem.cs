﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace KnifeHitClone.UI
{
    class KnifeUIItem : MonoBehaviour
    {
        [SerializeField] Button Button;
        [SerializeField] Image Image;
        [SerializeField] Text Text;
        [SerializeField] Color ActiveColor = Color.white;
        [SerializeField] Color DeactiveColor = new Color(0, 0, 0, .6f);
        [SerializeField] Color SelectedColor = Color.yellow;

        internal void Init(KnivesView.Item item, UnityAction callback)
        {
            Button.onClick.AddListener(callback);

            Text.text = item.knife.Cost.ToString();
            Text.gameObject.SetActive(!item.IsActive);

            Image.sprite = item.knife.Icon;
            Image.color = item.IsActive ? ActiveColor : DeactiveColor;
            if (item.isSelected)
            {
                var c = Button.colors;
                c.normalColor = SelectedColor;
                Button.colors = c;
            }
        }
    }
}
