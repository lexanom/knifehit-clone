﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "VibrationList", menuName = "Scriptables/VibrationList")]
    public class VibrationList : ScriptableObject
    {
        [Header("Паттерн вида \"вибр;пауза;вибр;пауза\" (мс)")]
        [SerializeField] string TargetHitPattern;
        [SerializeField] string KnifeHitPattern;
        [SerializeField] string AppleHitPattern;
        [SerializeField] string TargetDestroyPattern;

        [NonSerialized] public long[] TargetHit;
        [NonSerialized] public long[] KnifeHit;
        [NonSerialized] public long[] AppleHit;
        [NonSerialized] public long[] TargetDestroy;

        void OnEnable()
        {
            TargetHit = Parse(TargetHitPattern);
            KnifeHit = Parse(KnifeHitPattern);
            AppleHit = Parse(AppleHitPattern);
            TargetDestroy = Parse(TargetDestroyPattern);
        }

        long[] Parse(string pattern)
        {
            return pattern
                .Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => long.Parse(x))
                .ToArray();
        }
    }
}