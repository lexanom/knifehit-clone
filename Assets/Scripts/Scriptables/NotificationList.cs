﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "NotificationList", menuName = "Scriptables/NotificationList")]
    public class NotificationList : ScriptableObject
    {
        [Header("Настройки канала уведомлений")]
        public Channel ChannelSettings;
        [Header("Напоминание через N время после последнего выхода из приложения")]
        public bool NeedReminder = true;
        public Notify ReminderNotify;

        [Serializable]
        public class Channel
        {
            public string ChannelId = "KnifeHitClone";
            public string ChannelName = "KHc Name";
            public string ChannelDescription = "KHc Desc";
            public long[] VibrationPattern = new long[] { 50, 50, 50 };
        }

        [Serializable]
        public class Notify
        {
            public int Days;
            public int Hours;
            public int Min;
            [Space]
            public string Title;
            public string Text;
        }
    }
}