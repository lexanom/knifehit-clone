﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "SoundList", menuName = "Scriptables/SoundList")]
    public class SoundList : ScriptableObject
    {
        [SerializeField] string[] TargetHitSounds;
        [SerializeField] string[] KnifeHitSounds;
        [SerializeField] string[] AppleHitSounds;
        [SerializeField] string[] TargetDestroySounds;

        public SoundItem GetHitTarget()
        {
            return GetSoundItem(TargetHitSounds);
        }
        public SoundItem GetHitKnife()
        {
            return GetSoundItem(KnifeHitSounds);
        }
        public SoundItem GetHitApple()
        {
            return GetSoundItem(AppleHitSounds);
        }
        public SoundItem GetTargetDestroy()
        {
            return GetSoundItem(TargetDestroySounds);
        }

        #region service
        Dictionary<string, int> Cache;
        const string Folder = "Sounds";

        public class SoundItem
        {
            public int Id;
            public string FileName;

            public SoundItem(int id, string fileName)
            {
                Id = id;
                FileName = fileName;
            }
        }

        SoundItem GetSoundItem(string[] arr)
        {
            name = Path.Combine(Folder, arr[UnityEngine.Random.Range(0, arr.Length)]);
            if (Cache.TryGetValue(name, out int i))
                return new SoundItem(i, name);
            return null;
        }

        void CacheSound(string name)
        {
            name = Path.Combine(Folder, name);
            Cache[name] = AndroidNativeAudio.load(name);
        }

        void OnEnable()
        {
            Cache = new Dictionary<string, int>();
            AndroidNativeAudio.makePool();

            foreach (var item in KnifeHitSounds)
                CacheSound(item);
            foreach (var item in TargetHitSounds)
                CacheSound(item);
            foreach (var item in AppleHitSounds)
                CacheSound(item);
            foreach (var item in TargetDestroySounds)
                CacheSound(item);
        }

        void OnDestroy()
        {
            foreach (var item in Cache.Values)
                AndroidNativeAudio.unload(item);
            AndroidNativeAudio.releasePool();
        }
        #endregion
    }
}