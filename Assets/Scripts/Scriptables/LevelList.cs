﻿using System;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "LevelList", menuName = "Scriptables/LevelList")]
    public class LevelList : ScriptableObject
    {
        public Level[] List;
    }
}