﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "Target", menuName = "Scriptables/Models/Target")]
    public class Target : ScriptableObject
    {
        public Transform TargetTransform;
        public Transform DestroyTransform;
    }
}
