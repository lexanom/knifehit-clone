﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone
{
    public enum LevelType
    {
        [InspectorName("Обычный уровень")]
        Normal,
        [InspectorName("Босс")]
        Boss,
        [InspectorName("Супер босс (пока не реализовано)")]
        SuperBoss
    }

    [Serializable]
    public class Range
    {
        public int Min;
        public int Max;
    }

    [CreateAssetMenu(fileName = "Level", menuName = "Scriptables/Models/Level")]
    public class Level : ScriptableObject
    {
        [Serializable]
        public class BossSetting
        {
            public string Name;
            public Knife Reward;
        }

        /// <summary>
        /// Тип уровня (влияет на всплывашку в начале игры и на бонусы при победе)
        /// </summary>
        public LevelType LevelType = LevelType.Normal;

        /// <summary>
        /// Если уровень тип - босс
        /// </summary>
        public BossSetting BossSettings;

        /// <summary>
        /// Количество бросков
        /// </summary>
        public int Knives = 5;

        /// <summary>
        /// Мишень
        /// </summary>
        public Target Target;

        /// <summary>
        /// Кривая вращения мишени
        /// </summary>
        public AnimationCurve RotationCurve;

        /// <summary>
        /// Спаун ножей в количестве от-до
        /// </summary>
        public Range KnivesOnLevel;
    }
}
