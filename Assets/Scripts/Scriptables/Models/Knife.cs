﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "Knife", menuName = "Scriptables/Models/Knife")]
    public class Knife : ScriptableObject
    {
        [Tooltip("Иконка для меню выбора")]
        public Sprite Icon;
        [Tooltip("Префаб с Collider и Rigidbody")]
        public GameObject gameObject;
        [Tooltip("Стоимость покупки за яблоки")]
        public int Cost;

        [NonSerialized] public Game.Knife knife;

        private void OnEnable()
        {
            knife = gameObject.GetComponent<Game.Knife>();
            if (!knife)
                knife = gameObject.AddComponent<Game.Knife>();
        }
    }
}