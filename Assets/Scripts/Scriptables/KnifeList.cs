﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "KnifeList", menuName = "Scriptables/KnifeList")]
    public class KnifeList : ScriptableObject
    {
        public List<Knife> List;
    }
}