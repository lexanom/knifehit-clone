﻿using System;
using UnityEngine;

namespace KnifeHitClone
{
    [CreateAssetMenu(fileName = "GameOptions", menuName = "Scriptables/GameOptions")]
    public class GameOptions : ScriptableObject
    {
        /// <summary>
        /// Максимально возможное количество заспауненных предметов в мишени
        /// </summary>
        public int MaxSpawnItems = 20;

        /// <summary>
        /// Шанс выпадения яблока на уровне
        /// </summary>
        [Range(0, 1)]
        public float AppleSpawnChance = .25f;

        /// <summary>
        /// Макс скорость ножа
        /// </summary>
        public float KnifeSpeed = 0.2f;

        /// <summary>
        /// Стоимость повтора текущего уровня
        /// </summary>
        public int TryAgainCost = 20;
    }
}