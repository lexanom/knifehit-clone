﻿namespace KnifeHitClone.System
{
    public interface IVibration
    {
        /// <summary>
        /// Вибрировать в течение периода
        /// </summary>
        /// <param name="pattern">Длительность в мс, по-умолчанию если пусто</param>
        void Vibrate(params long[] pattern);

        /// <summary>
        /// Остановить вибрацию
        /// </summary>
        void Stop();
    }
}