﻿using System;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace KnifeHitClone.System
{
    /// <summary>
    /// https://github.com/BenoitFreslon/Vibration
    /// </summary>
    public class BFVibration : IVibration
    {
        public BFVibration()
        {
            Vibration.Init();
        }

        public async void Vibrate(params long[] pattern)
        {
            if (Storage.Settings.UseVibration)
            {
                await Task.Delay(100);

                if (pattern.Length == 0)
                    Vibration.Vibrate();

                else if (pattern.Length == 1)
                    Vibration.Vibrate(pattern[0]);

                else
                {
                    long[] array = pattern.Select(x => (long)x).ToArray();
                    Vibration.Vibrate(array, -1);
                }
            }
        }

        public void Stop()
        {
            Vibration.Cancel();
        }
    }
}