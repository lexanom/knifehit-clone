﻿using System;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

namespace KnifeHitClone.System
{
    /// <summary>
    /// Unity.MobileNotifications package
    /// </summary>
    public class MobileNotifications : INotify
    {
        public string ChannelId { get; private set; }

        public MobileNotifications(string channelId, string channelName, string channelDescription, Importance importance, bool enableVibration, LockScreenVisibility lockScreenVisibility, long[] vibrationPattern)
        {
            ChannelId = channelId;
#if UNITY_ANDROID
            AndroidNotificationCenter.RegisterNotificationChannel(new AndroidNotificationChannel
            {
                Id = channelId,
                Name = channelName,
                Description = channelDescription,
                Importance = importance,
                EnableVibration = enableVibration,
                LockScreenVisibility = lockScreenVisibility,
                VibrationPattern = vibrationPattern,
            });
#elif UNITY_IOS
            var authorizationOption = AuthorizationOption.Alert | AuthorizationOption.Badge;
            var req = new AuthorizationRequest(authorizationOption, true);
#endif
        }

        public void Cancel(int notifyId)
        {
#if UNITY_ANDROID
            AndroidNotificationCenter.CancelScheduledNotification(notifyId);
            AndroidNotificationCenter.CancelDisplayedNotification(notifyId);
#elif UNITY_IOS
            iOSNotificationCenter.RemoveScheduledNotification(identifier);
            iOSNotificationCenter.RemoveDeliveredNotification(identifier);
#endif
        }

        public int Send(DateTime fireTime, string title, string text)
        {
#if UNITY_ANDROID
            return AndroidNotificationCenter.SendNotification(new AndroidNotification
            {
                Title = title,
                Text = text,
                FireTime = fireTime,
            },
            ChannelId);
#elif UNITY_IOS
            var timeTrigger = new iOSNotificationCalendarTrigger()
            {
                Day = fireTime.Day,
                Month = fireTime.Month,
                Year = fireTime.Year,
                Hour = fireTime.Hour,
                Minute = fireTime.Minute,
                Second = fireTime.Second,
                Repeats = false
            };
            var notification = new iOSNotification()
            {
                Title = title,
                Body = text,
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = ChannelId,
                ThreadIdentifier = "thread1",
                Trigger = timeTrigger,
            };
            iOSNotificationCenter.ScheduleNotification(notification);
            return 0;
#endif
        }
    }
}