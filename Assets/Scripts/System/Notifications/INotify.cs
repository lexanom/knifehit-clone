﻿using System;

namespace KnifeHitClone.System
{
    public interface INotify
    {
        /// <summary>
        /// Запланировать уведомление
        /// </summary>
        /// <returns>Id</returns>
        int Send(DateTime fireTime, string title, string text);

        /// <summary>
        /// Отмена запланированного умедомления
        /// </summary>
        void Cancel(int notifyId);
    }
}