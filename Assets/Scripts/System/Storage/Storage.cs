﻿using System;
using UnityEngine;
using System.Linq;

namespace KnifeHitClone
{
    /// <summary>
    /// Хранилище пользовательских данных
    /// </summary>
    public class Storage
    {
        public static Prefs Prefs;
        public static Settings Settings;

        public static void Save()
        {
            // Имитируем хоть какую то защиту и храним данные в шифрованной строке
            string value = string.Empty;
            value += Prefs.MaxScore + separator;
            value += Prefs.MaxStage + separator;
            value += Prefs.Apples + separator;
            value += string.Join(",", Prefs.Knives) + separator;
            value += Prefs.CurrentKnife + separator;
            PlayerPrefs.SetString(pref_key, Crypt(value, secret));

            PlayerPrefs.SetInt("UseSound", Settings.UseSound ? 1 : 0);
            PlayerPrefs.SetInt("UseVibration", Settings.UseVibration ? 1 : 0);
            PlayerPrefs.SetInt("UseFX", Settings.UseFX ? 1 : 0);

            Debug.Log("All settings saved");
        }

        public static void Load()
        {
            var values = GetValues();

            Prefs = new Prefs
            (
                maxScore : SafeParseInt(values.ElementAtOrDefault(0)),
                maxStage : SafeParseInt(values.ElementAtOrDefault(1)),
                apples : SafeParseInt(values.ElementAtOrDefault(2)),
                knives : values.ElementAtOrDefault(3)?
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => SafeParseInt(x))
                    .ToList(),
                currentKnife : SafeParseInt(values.ElementAtOrDefault(4))
            );

            Settings = new Settings
            {
                UseSound = PlayerPrefs.GetInt("UseSound", 1) == 1,
                UseVibration = PlayerPrefs.GetInt("UseVibration", 1) == 1,
                UseFX = PlayerPrefs.GetInt("UseFX", 1) == 1,
            };

            Debug.Log("All settings loaded");
        }

        #region Service
        const string pref_key = "Prefs";
        const ushort secret = 35494;
        const string separator = ";";

        static Storage()
        {
            Load();
        }

        static string[] GetValues()
        {
            var str = PlayerPrefs.GetString(pref_key, string.Empty);

            if (string.IsNullOrWhiteSpace(str))
            {
                Debug.Log("Storaged settings not found");
                return new string[0];
            }

            str = Crypt(str, secret);
            return str.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        static int SafeParseInt(string s)
        {
            if (s is null)
                return 0;
            int.TryParse(s, out int i);
            return i;
        }

        static bool SafeParseBool(string s)
        {
            if (s is null)
                return false;
            bool.TryParse(s, out bool i);
            return i;
        }

        static string Crypt(string str, ushort key)
        {
            return string.Concat(str.Select(x => (char)(x ^ key)));
        }
        #endregion
    }
}
