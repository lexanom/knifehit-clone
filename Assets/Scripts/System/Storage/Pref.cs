﻿using System;
using System.Collections.Generic;

namespace KnifeHitClone
{
    public class Prefs
    {
        /// <summary>
        /// Внутриигровая валюта
        /// </summary>
        public int Apples { get; private set; }
        /// <summary>
        /// Рекорд очков
        /// </summary>
        public int MaxScore;
        /// <summary>
        /// Макс достигнутый уровень
        /// </summary>
        public int MaxStage;
        /// <summary>
        /// Id's доступных ножей
        /// </summary>
        public List<int> Knives;
        /// <summary>
        /// Текущий нож
        /// </summary>
        public int CurrentKnife;

        public Prefs(int apples, int maxScore, int maxStage, List<int> knives, int currentKnife)
        {
            Apples = apples;
            MaxScore = maxScore;
            MaxStage = maxStage;
            Knives = knives ?? new List<int> { 0 };
            CurrentKnife = currentKnife;
        }

        public bool SpendApples(int count, bool forceSave)
        {
            if (Apples < count)
                return false;
            Apples -= count;
            if (forceSave)
                Storage.Save();
            return true;
        }

        internal void AddApples(int count)
        {
            Apples += count;
        }
    }
}
