﻿namespace KnifeHitClone
{
    public class Settings
    {
        public bool UseSound = true;
        public bool UseVibration = true;
        public bool UseFX = true;
    }
}
