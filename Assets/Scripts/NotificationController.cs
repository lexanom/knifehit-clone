﻿using System;
using UnityEngine;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

namespace KnifeHitClone.System
{
    public class NotificationController : MonoBehaviour
    {
        INotify Notification;
        [SerializeField] NotificationList NotificationList;

        static bool Loaded;
        void Awake()
        {
            if (Loaded)
            {
                Destroy(this);
                return;
            }
            Loaded = true;

            var ch = NotificationList.ChannelSettings;
            Notification = new MobileNotifications(
                ch.ChannelId,
                ch.ChannelName,
                ch.ChannelDescription,
                Importance.Default,
                ch.VibrationPattern.Length > 0,
                LockScreenVisibility.Public,
                ch.VibrationPattern);
        }

        void OnApplicationPause(bool pause)
        {
            CheckAndSendReminderNotify(pause);
        }

        void OnApplicationQuit()
        {
            CheckAndSendReminderNotify(true);
        }

        void CheckAndSendReminderNotify(bool send)
        {
            string key = "RemNot";

            int id = PlayerPrefs.GetInt(key, -1);
            if (id >= 0)
            {
                Notification.Cancel(id);     // отменяем старое уведомление
                Debug.Log("reminder notify deleted");
            }

            if (send && NotificationList.NeedReminder)
            {
                var notify = NotificationList.ReminderNotify;
                var date = DateTime.Now.AddDays(notify.Days).AddHours(notify.Hours).AddMinutes(notify.Min);
                id = Notification.Send(date, notify.Title, notify.Text);

                PlayerPrefs.SetInt(key, id);
                Debug.Log("reminder notify sended");
            }
        }
    }
}