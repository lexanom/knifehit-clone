﻿using KnifeHitClone.System;
using KnifeHitClone.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KnifeHitClone
{
    public class GameController : MonoBehaviour
    {
        static bool Loaded;
        void Awake()
        {
            if (Loaded)
            {
                Destroy(this);
                return;
            }
            Loaded = true;
        }

        void OnApplicationQuit()
        {
            OnExit();
        }

        void OnApplicationPause(bool pause)
        {
            if (pause)
                OnExit();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                BackButton();
        }

        void OnExit()
        {
            Storage.Save();
        }

        void BackButton()
        {    
            //отходим на экран назад
            if (ScreenManager.ScreenCount > 1)
                ScreenManager.Pop();

            //сворачиваем приложение
            else
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    new AndroidJavaClass("com.unity3d.player.UnityPlayer")
                        .GetStatic<AndroidJavaObject>("currentActivity")
                        .Call<bool>("moveTaskToBack", true);
                }
                else
                    Application.Quit();
            }
        }
    }
}